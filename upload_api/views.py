from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound
from django.shortcuts import render_to_response
from django import forms
import hmac
import hashlib
import base64
import time
import os

def upload(request):
    return render_to_response('upload.html')
    
def hmac_sig(args):
    key = 'key'
    return hmac.new(key, args).digest().encode('base64').strip()
    
def check_sig(request):
    check_dict = {'device_id': 'hmac_id', 'log_type': 'hmac_type', 'upload_time': 'hmac_time'}
    for (key, value) in check_dict.items():
        if not hmac_sig(request.POST[key]) == request.POST[value]:
            return False
    return True
    
def upload_api(request):
    if check_sig(request):
        device_id = request.POST['device_id']
        log_type = request.POST['log_type']
        upload_time = request.POST['upload_time']
        today = time.strftime('%Y%m%d') 
        try:
            if not os.path.isdir('/var/www/example.com/data/%s/%s'%(log_type, today)):
                os.makedirs('%s/%s'%(log_type, today))
            file = open('/var/www/example.com/data/%s/%s/%s'%(log_type, today, device_id), 'wb+')
        except:
            return HttpResponseNotFound('Open file failed')
        if time.time() - float(upload_time) < 300:
            try:
                data = request.FILES.get('log_file').read()
            except:
                return HttpResponseNotFound('Missing log file')
            file.write(data)
            file.flush()
            file.close()
            return HttpResponse('OK')
        else:
             return HttpResponseForbidden('Timedout')           
    else:
        return HttpResponseForbidden('Wrong HMAC ID')